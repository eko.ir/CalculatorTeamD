package tugas.binar;

import java.util.ArrayList;
import java.util.Scanner;

class Calculator {
    private double hasil;
    private double sementara;
    private ArrayList<Double> nilai = new ArrayList<>();
    private Scanner scanner = new Scanner(System.in);

    private void arrayNum(int arr[]){
        int min = arr[0];
        int max = arr[0];
        int med = arr[0];

        for (int i = 1; i < arr.length; i++){
            if (arr[i] < min){
                min = arr[i];
            }
            if (arr[i] > max){
                max = arr[i];
            }
            med = arr[i] + med;
        }
        med = med / arr.length;
        System.out.println("Nilai Terbesar= " + max + "\n" +
                "Nilai Terkecil: " + min + "\n" +
                "Nilai Rata-rata: " + med);
    }


    private void hitungPenjumlahan(ArrayList<Double> nilai) {
        hasil = 0;
        for (double nl : nilai) {
            hasil += nl;
        }
    }

    private double getHasil() {
        return hasil;
    }

    private void hitungPengurangan(ArrayList<Double> nilai){
        hasil = 0;
        sementara =0;

        for (double n1 : nilai){
            if (hasil == 0){
                hasil = n1;
            }
            else{
                sementara = n1;
            }
        }
        hasil = (hasil-sementara);

    }

    private void inputNilai() {
        System.out.print("Masukkan nilai : ");
        nilai.add(scanner.nextDouble());
    }

    private void hitungPerkalian(ArrayList<Double> nilai) {
        hasil = nilai.get(0);
        for (int i = 1; i < nilai.size(); i++) {
            hasil *= nilai.get(i);
        }
    }

    private void hitungPembagian(ArrayList<Double> nilai){
        hasil = nilai.get(0);
        for (int   u = 1; u< nilai.size(); u++){
            hasil /= nilai.get(u);
        }
    }

    void menu(){
        System.out.print("Menu\n" +
                "1. Penjumlahan\n" +
                "2. Pengurangan\n" +
                "3. Perkalian\n" +
                "4. Pembagian\n" +
                "5. Nilai Terbesar, Terkecil dan Rata-rata \n" +
                "Masukkan pilihan anda: ");
        int choice = scanner.nextInt();

        switch (choice){
            case 1:
                inputNilai();
                inputNilai();
                hitungPenjumlahan(nilai);
                System.out.println("Hasil : " + getHasil());
                break;
            case 2:
                inputNilai();
                inputNilai();
                hitungPengurangan(nilai);
                System.out.println("Hasil : "+getHasil());
                break;
            case 3:
                inputNilai();
                inputNilai();
                hitungPerkalian(nilai);
                System.out.println("Hasil : " + getHasil());
                break;
            case 4:
                inputNilai();
                inputNilai();
                hitungPembagian(nilai);
                System.out.println("Hasil : "+getHasil());
                break;
            case 5:
                System.out.println("Banyak angka: ");
                int total = scanner.nextInt();
                int arr[] = new int[total];
                for (int i = 0; i < total;i++){
                    System.out.print("Nilai " + (i+1) + ": ");
                    arr[i] = scanner.nextInt();
                }
                arrayNum(arr);
                break;
        }

    }


}
